﻿namespace IT_2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.ChartSpectr = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ChartSignal = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button_Sdv = new System.Windows.Forms.Button();
            this.vosst = new System.Windows.Forms.Button();
            this.build_signal = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.error = new System.Windows.Forms.TextBox();
            this.t_vost = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.len = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.d5 = new System.Windows.Forms.TextBox();
            this.d4 = new System.Windows.Forms.TextBox();
            this.d3 = new System.Windows.Forms.TextBox();
            this.d2 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.d1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.x_5 = new System.Windows.Forms.TextBox();
            this.x_4 = new System.Windows.Forms.TextBox();
            this.x_3 = new System.Windows.Forms.TextBox();
            this.x_2 = new System.Windows.Forms.TextBox();
            this.x_1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.A2 = new System.Windows.Forms.TextBox();
            this.A3 = new System.Windows.Forms.TextBox();
            this.A4 = new System.Windows.Forms.TextBox();
            this.A5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.A1 = new System.Windows.Forms.TextBox();
            this.otr = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.timer_data = new System.Windows.Forms.Timer(this.components);
            this.timer_sdv = new System.Windows.Forms.Timer(this.components);
            this.button_stop = new System.Windows.Forms.Button();
            this.RESET = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSpectr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSignal)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ChartSpectr
            // 
            this.ChartSpectr.BorderlineColor = System.Drawing.Color.Transparent;
            chartArea1.AxisX2.MajorTickMark.Size = 0.1F;
            chartArea1.AxisX2.MajorTickMark.TickMarkStyle = System.Windows.Forms.DataVisualization.Charting.TickMarkStyle.InsideArea;
            chartArea1.Name = "ChartArea1";
            this.ChartSpectr.ChartAreas.Add(chartArea1);
            this.ChartSpectr.Location = new System.Drawing.Point(-20, 412);
            this.ChartSpectr.Margin = new System.Windows.Forms.Padding(5);
            this.ChartSpectr.Name = "ChartSpectr";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Name = "Series1";
            this.ChartSpectr.Series.Add(series1);
            this.ChartSpectr.Size = new System.Drawing.Size(857, 252);
            this.ChartSpectr.TabIndex = 94;
            // 
            // ChartSignal
            // 
            this.ChartSignal.BorderlineColor = System.Drawing.Color.Transparent;
            chartArea2.Name = "ChartArea1";
            this.ChartSignal.ChartAreas.Add(chartArea2);
            this.ChartSignal.Location = new System.Drawing.Point(-5, -9);
            this.ChartSignal.Margin = new System.Windows.Forms.Padding(5);
            this.ChartSignal.Name = "ChartSignal";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Name = "Series1";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Name = "Series2";
            this.ChartSignal.Series.Add(series2);
            this.ChartSignal.Series.Add(series3);
            this.ChartSignal.Size = new System.Drawing.Size(842, 346);
            this.ChartSignal.TabIndex = 93;
            // 
            // button_Sdv
            // 
            this.button_Sdv.Enabled = false;
            this.button_Sdv.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button_Sdv.Location = new System.Drawing.Point(854, 611);
            this.button_Sdv.Margin = new System.Windows.Forms.Padding(5);
            this.button_Sdv.Name = "button_Sdv";
            this.button_Sdv.Size = new System.Drawing.Size(128, 49);
            this.button_Sdv.TabIndex = 92;
            this.button_Sdv.Text = "Сдвиг";
            this.button_Sdv.UseVisualStyleBackColor = true;
            this.button_Sdv.Click += new System.EventHandler(this.button_Sdv_Click);
            // 
            // vosst
            // 
            this.vosst.Location = new System.Drawing.Point(854, 534);
            this.vosst.Name = "vosst";
            this.vosst.Size = new System.Drawing.Size(128, 51);
            this.vosst.TabIndex = 91;
            this.vosst.Text = "Восстановить";
            this.vosst.UseVisualStyleBackColor = true;
            this.vosst.Click += new System.EventHandler(this.vosst_Click);
            // 
            // build_signal
            // 
            this.build_signal.Location = new System.Drawing.Point(853, 464);
            this.build_signal.Name = "build_signal";
            this.build_signal.Size = new System.Drawing.Size(128, 51);
            this.build_signal.TabIndex = 90;
            this.build_signal.Text = "Построить графики";
            this.build_signal.UseVisualStyleBackColor = true;
            this.build_signal.Click += new System.EventHandler(this.build_signal_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(872, 405);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 17);
            this.label20.TabIndex = 87;
            this.label20.Text = "Невязки";
            // 
            // error
            // 
            this.error.Location = new System.Drawing.Point(854, 425);
            this.error.Name = "error";
            this.error.Size = new System.Drawing.Size(100, 22);
            this.error.TabIndex = 86;
            // 
            // t_vost
            // 
            this.t_vost.Location = new System.Drawing.Point(1011, 425);
            this.t_vost.Name = "t_vost";
            this.t_vost.Size = new System.Drawing.Size(100, 22);
            this.t_vost.TabIndex = 85;
            this.t_vost.Text = "8";
            this.t_vost.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(1008, 405);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(244, 17);
            this.label19.TabIndex = 84;
            this.label19.Text = "Точность восстановления(степень)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(219, 372);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(254, 17);
            this.label17.TabIndex = 83;
            this.label17.Text = "Исходный и восстановленный сигнал";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.len);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.d5);
            this.groupBox1.Controls.Add(this.d4);
            this.groupBox1.Controls.Add(this.d3);
            this.groupBox1.Controls.Add(this.d2);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.d1);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.x_5);
            this.groupBox1.Controls.Add(this.x_4);
            this.groupBox1.Controls.Add(this.x_3);
            this.groupBox1.Controls.Add(this.x_2);
            this.groupBox1.Controls.Add(this.x_1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.A2);
            this.groupBox1.Controls.Add(this.A3);
            this.groupBox1.Controls.Add(this.A4);
            this.groupBox1.Controls.Add(this.A5);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.A1);
            this.groupBox1.Location = new System.Drawing.Point(845, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(440, 334);
            this.groupBox1.TabIndex = 82;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Характеристики сигнала";
            // 
            // len
            // 
            this.len.Location = new System.Drawing.Point(143, 267);
            this.len.Name = "len";
            this.len.Size = new System.Drawing.Size(100, 22);
            this.len.TabIndex = 46;
            this.len.Text = "512";
            this.len.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 270);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(107, 17);
            this.label23.TabIndex = 45;
            this.label23.Text = "Длина сигнала";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(285, 201);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(121, 17);
            this.label16.TabIndex = 40;
            this.label16.Text = "Ширина купола 5";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(285, 156);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(121, 17);
            this.label15.TabIndex = 40;
            this.label15.Text = "Ширина купола 4";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(285, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(121, 17);
            this.label14.TabIndex = 44;
            this.label14.Text = "Ширина купола 3";
            // 
            // d5
            // 
            this.d5.Location = new System.Drawing.Point(288, 221);
            this.d5.Name = "d5";
            this.d5.Size = new System.Drawing.Size(100, 22);
            this.d5.TabIndex = 43;
            this.d5.Text = "1";
            this.d5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // d4
            // 
            this.d4.Location = new System.Drawing.Point(288, 176);
            this.d4.Name = "d4";
            this.d4.Size = new System.Drawing.Size(100, 22);
            this.d4.TabIndex = 42;
            this.d4.Text = "1";
            this.d4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // d3
            // 
            this.d3.Location = new System.Drawing.Point(288, 126);
            this.d3.Name = "d3";
            this.d3.Size = new System.Drawing.Size(100, 22);
            this.d3.TabIndex = 41;
            this.d3.Text = "1";
            this.d3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // d2
            // 
            this.d2.Location = new System.Drawing.Point(288, 84);
            this.d2.Name = "d2";
            this.d2.Size = new System.Drawing.Size(100, 22);
            this.d2.TabIndex = 40;
            this.d2.Text = "1";
            this.d2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(285, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 17);
            this.label13.TabIndex = 39;
            this.label13.Text = "Ширина купола 2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(285, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 17);
            this.label9.TabIndex = 38;
            this.label9.Text = "Ширина купола 1";
            // 
            // d1
            // 
            this.d1.Location = new System.Drawing.Point(288, 39);
            this.d1.Name = "d1";
            this.d1.Size = new System.Drawing.Size(100, 22);
            this.d1.TabIndex = 37;
            this.d1.Text = "1";
            this.d1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(116, 201);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(148, 17);
            this.label12.TabIndex = 36;
            this.label12.Text = "Распожение центра5";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(116, 151);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(148, 17);
            this.label11.TabIndex = 35;
            this.label11.Text = "Распожение центра4";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(116, 106);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(148, 17);
            this.label10.TabIndex = 34;
            this.label10.Text = "Распожение центра3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(116, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(148, 17);
            this.label7.TabIndex = 29;
            this.label7.Text = "Распожение центра2";
            // 
            // x_5
            // 
            this.x_5.Location = new System.Drawing.Point(143, 221);
            this.x_5.Name = "x_5";
            this.x_5.Size = new System.Drawing.Size(100, 22);
            this.x_5.TabIndex = 28;
            this.x_5.Text = "487";
            this.x_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // x_4
            // 
            this.x_4.Location = new System.Drawing.Point(143, 176);
            this.x_4.Name = "x_4";
            this.x_4.Size = new System.Drawing.Size(100, 22);
            this.x_4.TabIndex = 27;
            this.x_4.Text = "356";
            this.x_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // x_3
            // 
            this.x_3.Location = new System.Drawing.Point(143, 126);
            this.x_3.Name = "x_3";
            this.x_3.Size = new System.Drawing.Size(100, 22);
            this.x_3.TabIndex = 26;
            this.x_3.Text = "214";
            this.x_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // x_2
            // 
            this.x_2.Location = new System.Drawing.Point(143, 84);
            this.x_2.Name = "x_2";
            this.x_2.Size = new System.Drawing.Size(100, 22);
            this.x_2.TabIndex = 25;
            this.x_2.Text = "104";
            this.x_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // x_1
            // 
            this.x_1.Location = new System.Drawing.Point(143, 39);
            this.x_1.Name = "x_1";
            this.x_1.Size = new System.Drawing.Size(100, 22);
            this.x_1.TabIndex = 24;
            this.x_1.Text = "54";
            this.x_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(116, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(148, 17);
            this.label8.TabIndex = 21;
            this.label8.Text = "Распожение центра1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 17);
            this.label6.TabIndex = 20;
            this.label6.Text = "Амплитуда4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 201);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 17);
            this.label4.TabIndex = 19;
            this.label4.Text = "Амплитуда5";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 17);
            this.label3.TabIndex = 18;
            this.label3.Text = "Амплитуда2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 17);
            this.label2.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Амплитуда3";
            // 
            // A2
            // 
            this.A2.Location = new System.Drawing.Point(6, 84);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(100, 22);
            this.A2.TabIndex = 15;
            this.A2.Text = "4";
            this.A2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // A3
            // 
            this.A3.Location = new System.Drawing.Point(6, 126);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(100, 22);
            this.A3.TabIndex = 14;
            this.A3.Text = "3";
            this.A3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // A4
            // 
            this.A4.Location = new System.Drawing.Point(6, 176);
            this.A4.Name = "A4";
            this.A4.Size = new System.Drawing.Size(100, 22);
            this.A4.TabIndex = 13;
            this.A4.Text = "2";
            this.A4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // A5
            // 
            this.A5.Location = new System.Drawing.Point(9, 221);
            this.A5.Name = "A5";
            this.A5.Size = new System.Drawing.Size(100, 22);
            this.A5.TabIndex = 12;
            this.A5.Text = "1";
            this.A5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Амплитуда1";
            // 
            // A1
            // 
            this.A1.Location = new System.Drawing.Point(6, 39);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(100, 22);
            this.A1.TabIndex = 6;
            this.A1.Text = "5";
            this.A1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // otr
            // 
            this.otr.AutoSize = true;
            this.otr.Location = new System.Drawing.Point(851, 354);
            this.otr.Name = "otr";
            this.otr.Size = new System.Drawing.Size(335, 21);
            this.otr.TabIndex = 47;
            this.otr.Text = "Отразить восстановленный сигнал зеркально";
            this.otr.UseVisualStyleBackColor = true;
            this.otr.CheckedChanged += new System.EventHandler(this.otr_CheckedChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(292, 669);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(111, 17);
            this.label18.TabIndex = 95;
            this.label18.Text = "Спектр сигнала";
            // 
            // timer_data
            // 
            this.timer_data.Tick += new System.EventHandler(this.timer_data_Tick);
            // 
            // timer_sdv
            // 
            this.timer_sdv.Tick += new System.EventHandler(this.timer_sdv_Tick);
            // 
            // button_stop
            // 
            this.button_stop.Location = new System.Drawing.Point(1011, 464);
            this.button_stop.Name = "button_stop";
            this.button_stop.Size = new System.Drawing.Size(128, 51);
            this.button_stop.TabIndex = 96;
            this.button_stop.Text = "Остановить ";
            this.button_stop.UseVisualStyleBackColor = true;
            this.button_stop.Click += new System.EventHandler(this.button_stop_Click);
            // 
            // RESET
            // 
            this.RESET.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.RESET.Location = new System.Drawing.Point(1011, 537);
            this.RESET.Name = "RESET";
            this.RESET.Size = new System.Drawing.Size(128, 48);
            this.RESET.TabIndex = 97;
            this.RESET.Text = "Сброс  ";
            this.RESET.UseVisualStyleBackColor = true;
            this.RESET.Click += new System.EventHandler(this.RESET_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 729);
            this.Controls.Add(this.RESET);
            this.Controls.Add(this.button_stop);
            this.Controls.Add(this.otr);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.ChartSpectr);
            this.Controls.Add(this.ChartSignal);
            this.Controls.Add(this.button_Sdv);
            this.Controls.Add(this.vosst);
            this.Controls.Add(this.build_signal);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.error);
            this.Controls.Add(this.t_vost);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ChartSpectr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSignal)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart ChartSpectr;
        private System.Windows.Forms.DataVisualization.Charting.Chart ChartSignal;
        private System.Windows.Forms.Button button_Sdv;
        private System.Windows.Forms.Button vosst;
        private System.Windows.Forms.Button build_signal;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox error;
        private System.Windows.Forms.TextBox t_vost;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox len;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox d5;
        private System.Windows.Forms.TextBox d4;
        private System.Windows.Forms.TextBox d3;
        private System.Windows.Forms.TextBox d2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox d1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox x_5;
        private System.Windows.Forms.TextBox x_4;
        private System.Windows.Forms.TextBox x_3;
        private System.Windows.Forms.TextBox x_2;
        private System.Windows.Forms.TextBox x_1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox A2;
        private System.Windows.Forms.TextBox A3;
        private System.Windows.Forms.TextBox A4;
        private System.Windows.Forms.TextBox A5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox A1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Timer timer_data;
        private System.Windows.Forms.Timer timer_sdv;
        private System.Windows.Forms.CheckBox otr;
        private System.Windows.Forms.Button button_stop;
        private System.Windows.Forms.Button RESET;
    }
}

