﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;


namespace IT_2
{
    public partial class Form1 : Form
    {
       
        Int32 calc;
        UInt16 len_s;//количество отсчетов
        Double TAU, Er, Eps;
        double A_1, A_2, A_3, A_4, A_5,// параметры сигнала 
               D1, D2, D3, D4, D5;
        UInt16 X1, X2, X3, X4, X5;
        //  int index_min, raznost;

        bool Timer1, init, stop_Timer1;// переменные к анимации
        bool Timer2, stop_Timer2;

      

        double modul;

      

        Double[] Signal;//массив для сигнала 

        private void Form1_Load(object sender, EventArgs e)
        {
            RESET_Click(sender, e);
        }

        Double[] Spectr;//массив для спектра 

       
        Double[] Vosst;
        Double[] Signal_vosst;//Массив восстановленного сигнала
        Cmplx[] F_Vosst;//комплексный массив
      

        private Data FormStore;
       
        public Form1()
        {
            InitializeComponent();
            timer_data.Interval = 1;
            timer_sdv.Interval = 1;

            FormStore = new Data();
            stop_Timer2 = false;
            init = true;
            Timer1= true;
            Timer2 = true;
            calc = 0;

        }


        void Init()
        {
            vosst.Enabled = false;
            button_Sdv.Enabled = false;

        }
        public struct Cmplx//структура для комплексного числа
        {
            public double real; public double image;
            public Cmplx(double param1, double param2)
            {
                real = param1;
                image = param2;
            }
        }
        public static void Fourea(Cmplx[] sig, int n, int s)// функция для Фурье- преобразований, s=1 - обратное, s=-1 -прямое
        {
            int i, j, istep;
            int m, mmax;
            double r, r1, theta, w_r, w_i, temp_r, temp_i;
            double pi = 3.1415926f;

            r = pi * s;
            j = 0;
            for (i = 0; i < n; i++)
            {
                if (i < j)
                {

                    temp_r = sig[j].real;
                    temp_i = sig[j].image;
                    sig[j].real = sig[i].real;
                    sig[j].image = sig[i].image;
                    sig[i].real = temp_r;
                    sig[i].image = temp_i;
                }
                m = n >> 1;
                while (j >= m) { j -= m; m = (m + 1) / 2; }
                j += m;
            }
            mmax = 1;
            while (mmax < n)
            {
                istep = mmax << 1;
                r1 = r / (double)mmax;
                for (m = 0; m < mmax; m++)
                {
                    theta = r1 * m;
                    w_r = (double)Math.Cos((double)theta);
                    w_i = (double)Math.Sin((double)theta);
                    for (i = m; i < n; i += istep)
                    {

                        j = i + mmax;
                        temp_r = w_r * sig[j].real - w_i * sig[j].image;
                        temp_i = w_r * sig[j].image + w_i * sig[j].real;
                        sig[j].real = sig[i].real - temp_r;
                        sig[j].image = sig[i].image - temp_i;
                        sig[i].real += temp_r;
                        sig[i].image += temp_i;

                    }
                }
                mmax = istep;
            }
            if (s > 0)
                for (i = 0; i < n; i++)
                {
                    sig[i].real /= (Convert.ToDouble(n));
                    sig[i].image /= (Convert.ToDouble(n));
                }

        }
        public Double[] Func_Spectr(UInt16 Length, Double[] Signal)//функция для нахлждения модуля спектра заданного сигнала
        {
            Cmplx[] Cmplx = new Cmplx[Length + 1];
            Double[] Spectr = new double[Length + 1];

            for (int i = 0; i < Length; i++)
            {
                Cmplx[i].real = (float)Signal[i];
                Cmplx[i].image = 0;
            }
            Fourea(Cmplx, Length, -1);
            for (int i = 0; i < Length; i++)
            {
                Spectr[i] = Math.Sqrt(Cmplx[i].real * Cmplx[i].real + Cmplx[i].image * Cmplx[i].image);
            }
            return Spectr;
        }



        private void build_signal_Click(object sender, EventArgs e)
        {
            Init();
            len_s = UInt16.Parse(len.Text);
            A_1 = Double.Parse(A1.Text); A_2 = Double.Parse(A2.Text); A_3 = Double.Parse(A3.Text); A_4 = Double.Parse(A4.Text); A_5 = Double.Parse(A5.Text);// Достаем данные 
            X1 = UInt16.Parse(x_1.Text); X2 = UInt16.Parse(x_2.Text); X3 = UInt16.Parse(x_3.Text); X4 = UInt16.Parse(x_4.Text); X5 = UInt16.Parse(x_5.Text);//для генерируемого сигнала
            D1 = Double.Parse(d1.Text); D2 = Double.Parse(d2.Text); D3 = Double.Parse(d3.Text); D4 = Double.Parse(d4.Text); D5 = Double.Parse(d5.Text);//из текстбоксов

            Signal = FormStore.GenSignal(len_s, A_1, A_2, A_3, A_4, A_5, D1, D2, D3, D4, D5, X1, X2, X3, X4, X5);//генерируем сигнал
            Spectr = Func_Spectr(len_s, Signal);//вычисляем модуль спектра 

            ChartSignal.Series[0].Points.Clear();//очищаем чарты
            ChartSpectr.Series[0].Points.Clear();

            for (int i = 1; i < len_s; i++)
            {
                ChartSignal.Series[0].Points.AddXY(i, Signal[i]);//отрисовка сигнала
            }
            for (int i = 1; i < len_s; i++)
            {
                ChartSpectr.Series[0].Points.AddXY(i/(double)1000, Spectr[i]);//отрисовка спектра
            }
            vosst.Enabled = true;

            ChartSignal.Invalidate();
            ChartSpectr.Invalidate();//для перерисовки графиков
            
        }


        public static void InitRec(UInt16 len, UInt16 Prec, Double[] Spect, Cmplx[] F_Vosst, Double[] Vosst, Double[] Signal_Vosst)
        {

            var min = 0.0;//диапазон для генерации чисел
            var max = 2 * Math.PI;// в интервале от 0 до 2Пи
            double phase = 0;
            var rnd = new Random();//элемент класса Random
            F_Vosst = new Cmplx[len];
          

            for (int i = 0; i < len; i++)
            {
                phase = rnd.NextDouble() * (max - min) + min;//генерируем фазу от 0 до 2пи
                F_Vosst[i].real = Spect[i]* Math.Cos(phase);
                F_Vosst[i].image =Spect[i] * Math.Sin(phase);
            }

            Fourea(F_Vosst, len, 1);
             

            for (int i = 0; i < len; i++)
            {
                Vosst[i] = F_Vosst[i].real;
                if (Vosst[i] < 0)
                    Vosst[i] = 0;
                   Signal_Vosst[i] = Vosst[i];
            }
          
        }

       


        private void button_Sdv_Click(object sender, EventArgs e)//кнопка сдвиг
        {
            if (Timer2 == true)
            {
                stop_Timer2 = false;
                timer_sdv.Enabled = true;
                Timer2 = false;
                button_Sdv.Text = "Стоп";
            }
            else
            {
               timer_sdv.Enabled = false;
               Timer2 = true;
                button_Sdv.Text = "Сдвиг";
            }


        }

        private void timer_data_Tick(object sender, EventArgs e)//Таймер для восстановления сигнала
          {

                  for (int i = 0; i < len_s; i++)
                  {
                      F_Vosst[i].real = Vosst[i];
                      F_Vosst[i].image = 0;
                  }

                  Fourea(F_Vosst, len_s, -1);// прямое преобразование Фурье

                  for (int i = 0; i < len_s; i++)
                  {
                     modul = Math.Sqrt(F_Vosst[i].real * F_Vosst[i].real + F_Vosst[i].image * F_Vosst[i].image); //новый модуль
                      F_Vosst[i].real = F_Vosst[i].real * Spectr[i] / modul; // меняем модуль комплексного числа
                      F_Vosst[i].image = F_Vosst[i].image * Spectr[i] / modul;// на модуль исходного сигнала
              }

                  Fourea(F_Vosst,len_s, 1); //обратное преобразование Фурье


              for (int i = 0; i < len_s; i++)//зануляем отрицательную часть воостановленого сигнала
              {
                  Vosst[i] = F_Vosst[i].real;
                  if (Vosst[i] < 0)
                      Vosst[i] = 0;

              }

              Er = 0;
              //считаем СКО
                  for (int i = 0; i <len_s; i++)
                  {
                      Er += (Signal_vosst[i]-Vosst[i]) * (Signal_vosst[i] - Vosst[i]) / len_s;
                  }

                  for (int i = 0; i <len_s; i++)//запоминаем предыдущее значение, чтобы использовать на следующей итерации
                  {
                      Signal_vosst[i] = Vosst[i];
                  }
              Int32 Prec= Int32.Parse(t_vost.Text);//степень точности вычислений, вынимаем из текстбокса
              TAU = Math.Pow(10.0, (-(double)Prec));  // Точность вычислений    
              if (Er < TAU)//пока наше СКО отклонение не станет меньше задданой точности, то продолжаем процесс
                 {
                    stop_Timer1= true;
                 }


              calc++;//счетчик итераций данного цикла
              ChartSignal.Series[1].Points.Clear();
              for (int i = 1; i <len_s; i++)
              {
                  ChartSignal.Series[1].Points.AddXY(i, Signal_vosst[i]);//отрисовываем восстановленный сигнал
              }

            if (stop_Timer1 == true)
            {
                timer_data.Enabled = false;
                vosst.Enabled = false;
                button_Sdv.Enabled = true;
               
                button_stop.Text = "Остановить";
                MessageBox.Show(
                   "The process of recovery have finished working after " + calc + " iterations.",
                   "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
         


        }

        private void timer_sdv_Tick(object sender, EventArgs e)//таймер для автоматического сдвига
          {
              len_s = UInt16.Parse(len.Text);
          
            ChartSignal.Series[1].Points.Clear();

            for (int i = 0; i < len_s; i++)
            {
                if (i + 1 < len_s)
                    Signal_vosst[i] = Signal_vosst[i + 1];
                else
                    Signal_vosst[i] = Signal_vosst[len_s - i + 1];
            }

            for (int i = 1; i < len_s; i++)
            {
                ChartSignal.Series[1].Points.AddXY(i, Signal_vosst[i]);//отрисовка
            }

            Eps = 0;
            for (int i = 0; i < len_s; i++)
            {
                Eps += (Signal[i] - Signal_vosst[i]) * (Signal[i] - Signal_vosst[i]) / len_s;//СКО
            }
          
            if (stop_Timer2 == true)
            {
                timer_sdv.Enabled = false;
                Timer2 = false;
                button_Sdv.Text = "Сдвиг";
            }

            error.Text = String.Format("{0:E}", Eps);//Выводит невязки на экран
          
        }
      
        private void otr_CheckedChanged(object sender, EventArgs e)//чекбокс отразить сигнал зеркально
        {
            ChartSignal.Series[1].Points.Clear();

            double buf;
            for (int i = 0; i < len_s / 2; i++)
            {
                buf =Signal_vosst[i];
                Signal_vosst[i] = Signal_vosst[len_s - i - 1];
                Signal_vosst[len_s- i - 1] = buf;
            }

            for (int i = 1; i < len_s; i++)
            {
                ChartSignal.Series[1].Points.AddXY(i, Signal_vosst[i]);
            }
        }
       
        private void button_stop_Click(object sender, EventArgs e)
        {
            if (Timer1 == true)
            {
                stop_Timer1 = false;
                timer_data.Enabled = true;
                Timer1 = false;
                button_stop.Text = "Остановить";
                
            }
            else
            {
                timer_data.Enabled = false;
                Timer1 = true;
                button_stop.Text = "Продолжить";
                MessageBox.Show(
                "The process of recovery have been cancelled after " + calc + " iterations.",
                "Abort", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }
      
        private void RESET_Click(object sender, EventArgs e)
        {
          
            calc = 0;
            Er = 0;
            Eps = 0;

            ChartSignal.Series[1].Points.Clear();

            init = true;
            timer_data.Enabled = false;
            timer_sdv.Enabled = false;
            Timer1 = true;
            stop_Timer1 = false;
            Timer2 = true;
            stop_Timer2 = false;
            vosst.Enabled = true;
            build_signal.Enabled = true;

            button_Sdv.Text = "Сдвиг";
            button_Sdv.Enabled = false;
        }
        private void vosst_Click(object sender, EventArgs e)
        {
            timer_sdv.Enabled = false;
            vosst.Enabled = true;
            timer_data.Enabled = true;
            UInt16 Prec = UInt16.Parse(t_vost.Text);//степень точности вычислений
            len_s = UInt16.Parse(len.Text);//длина сигнала, вынимаем из текстбокса
            Signal_vosst = new Double[len_s]; //наш восстановленный сигнал
            F_Vosst = new Cmplx[len_s];//комплексный массив для восстановления
            Vosst = new Double[len_s];

            if (init == true)
            {
                InitRec(len_s, Prec, Spectr, F_Vosst, Vosst, Signal_vosst);// Инцилизация для выполнения алгоритма
                init = false;
            }
            build_signal.Enabled = false;

            RESET.Enabled = true;

           


        }
    }
    }
