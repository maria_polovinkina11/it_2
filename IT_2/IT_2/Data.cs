﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IT_2
{
    class Data
    {
        private UInt16 Data_InSize;
        private Double[] Data_Ampl;
        private Double[] Data_Disp;
        private UInt16[] Data_Pos;

        // Конструктор:
        public Data()
        {
            Data_InSize = 5;
            Data_Ampl = new Double[Data_InSize];
            Data_Disp = new Double[Data_InSize];
            Data_Pos = new UInt16[Data_InSize];

        }
        public Double[] Get_Ampl(double a1, double a2, double a3, double a4, double a5)
        {
            Data_Ampl[0] = a1;
            Data_Ampl[1] = a2;
            Data_Ampl[2] = a3;
            Data_Ampl[3] = a4;
            Data_Ampl[4] = a5;
            return Data_Ampl;

        }
        public UInt16[] Get_Pos(UInt16 x1, UInt16 x2, UInt16 x3, UInt16 x4, UInt16 x5)
        {
            Data_Pos[0] = x1;
            Data_Pos[1] = x2;
            Data_Pos[2] = x3;
            Data_Pos[3] = x4;
            Data_Pos[4] = x5;
            return Data_Pos;

        }
        public Double[] Get_Disp(double d1, double d2, double d3, double d4, double d5)
        {
            Data_Disp[0] = d1;
            Data_Disp[1] = d2;
            Data_Disp[2] = d3;
            Data_Disp[3] = d4;
            Data_Disp[4] = d5;
            return Data_Disp;
        }

        public Double[] GenSignal(UInt16 Length, Double a1, Double a2, Double a3, Double a4, Double a5,
                                  Double d1, Double d2, Double d3, double d4, double d5,
                                  UInt16 x1, UInt16 x2, UInt16 x3, UInt16 x4, UInt16 x5)
        {
            Get_Ampl(a1, a2, a3, a4, a5);
            Get_Pos(x1, x2, x3, x4, x5);
            Get_Disp(d1, d2, d3, d4, d5);
            Double[] Signal = new Double[Length];
            for (UInt16 i = 0; i < Length; i++)
            {
                Signal[i] = 0.0;
                Double Pow = 0.0;
                for (UInt16 k = 0; k < Data_InSize; k++)
                {
                    if (Data_Ampl[k] > 0.0)
                    {
                        Pow = (Double)(i - Data_Pos[k]) / Data_Disp[k];
                        Pow = -Pow * Pow;
                        Signal[i] += Data_Ampl[k] * Math.Exp(Pow);
                    }
                }
            }
            return Signal;
        }
    }
}
